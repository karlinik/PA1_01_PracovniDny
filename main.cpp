#ifndef __PROGTEST__
#include <stdio.h>
#include <assert.h>
#endif /* __PROGTEST__ */

using namespace std;

//pridat fci vypis, jestli je nebo neni pracovni den
//dodelat fci na svatky

int IsWorkDay ( int y, int m, int d );
int CountWorkDays ( int y1, int m1, int d1, int y2, int m2, int d2, int * cnt );
int isLeap(int y);
int IsHoliday(int daycount);
int countDays(int y, int m, int d);
int reverseDay(int countD, int *y, int *m, int *d);
/*-------------------------------------------------*/
int IsHoliday(int daycount)
{
    int y,m,d;
    reverseDay(daycount,&y,&m,&d);
    if ((d==1 && m==1)||(d==1 && m==5)||(d==8 && m==5)
        ||(d==5 && m==7)||(d==6 && m==7)||(d==28 && m==9)
        ||(d==28 && m==10)||(d==17 && m==11)||(d==24 && m==12)
        ||(d==25 && m==12)||(d==26 && m==12))
    {
        return 1;
    } else return 0;
}
/*-------------------------------------------------*/
int reverseDay(int countD, int *y, int *m, int *d)
{
    int days, year,month;
    days=365;
    year=1900;
    while(countD>days)
    {
        countD-=days;
        year++;
        if (isLeap(year))
            {days=366;}
            else
            {days=365;}
    }
    *y=year;

    days=31;
    month=1;
    while(countD>days)
    {
        countD-=days;
        month++;
        if (month==4 ||month==6 || month==9 || month==11) days=30;
        else if (month==2 && isLeap(year)) days=29;
        else if (month==2) days=28;
        else days=31;
    }
    *m=month;
    *d=countD;
    return 0;
}
/*-------------------------------------------------*/
int IsWorkDay (int y, int m, int d)
{
    int day=0;
    day=countDays(y,m,d);
    if (day)
    {
        if (day%7==6 || day%7==0 || IsHoliday(day))
          return 0;
	else
	  return 1;
    }
return 0;
}
/*--------------------------------------------------*/
int CountWorkDays ( int y1, int m1, int d1,
                    int y2, int m2, int d2,
                    int * cnt )
{
    *cnt=0;
    int d,m,y,i, counter2=0;
    int day1=0, day2=0;
    day1=countDays(y1,m1,d1);
    day2=countDays(y2,m2,d2);
    if (day1>day2 || day1==0 ||day2==0)
        return 0;
    //i=day1;

    for (i=day1;i<=day2;i++)
    {
        reverseDay(i,&y,&m,&d);
        //printf("%d %d %d", )
        if (IsWorkDay(y,m,d))
        {
            //printf("ANO ++1")
            counter2++;
        }
    }
    *cnt=counter2;
    return 1;
}
/*---------------------------------------------------*/
int countDays(int y, int m, int d)
{
int countD;
int i;
int month[]={31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

//conditions
if (y<2000 || m<1 || m>12 || d<1 || d>31) return 0;
if ((m==4 ||m==6 || m==9 || m==11) && d>30) return 0;
if (m==2 && isLeap(y))
{
    if (d>29) return 0;
}
else if (m==2 && d>28) return 0;

//main part
countD=d;
for (i = 1900; i < y; i++)
        {
            if (isLeap(i))
            {countD+= 366;}
            else
            {countD+= 365;}
        }

if (isLeap(y)) month[1]=29;
for (i = 0; i < m-1; i++)
{
 countD+= month[i];
}
//printf("dny: %d\n", d);
return countD;
}
/*---------------------------------------------------*/
int isLeap(int y) {
    if (y % 4000 == 0) return 0;
    else if (y % 400 == 0) return 1;
    else if (y % 100 == 0) return 0;
    else if (y % 4 == 0) return 1;
    else return 0;
}

#ifndef __PROGTEST__
int main( int argc, char * argv [] )
{
int cnt;

  assert ( IsWorkDay ( 2016, 11, 11 ) );
  assert ( ! IsWorkDay ( 2016, 11, 12 ) );
  assert ( ! IsWorkDay ( 2016, 11, 17 ) );
  assert ( ! IsWorkDay ( 2016, 11, 31 ) );
  assert ( IsWorkDay ( 2016,  2, 29 ) );
  assert ( ! IsWorkDay ( 2004,  2, 29 ) );
  assert ( ! IsWorkDay ( 2001,  2, 29 ) );
  assert ( ! IsWorkDay ( 1996,  1,  1 ) );
  assert ( CountWorkDays ( 2016, 11,  1, 2016, 11, 30, &cnt ) == 1 && cnt == 21 );
  assert ( CountWorkDays ( 2016, 11,  1, 2016, 11, 17, &cnt ) == 1 && cnt == 12 );
  assert ( CountWorkDays ( 2016, 11,  1, 2016, 11,  1, &cnt ) == 1 && cnt == 1 );
  assert ( CountWorkDays ( 2016, 11, 17,2016, 11, 17, &cnt ) == 1&& cnt == 0 );
  assert ( CountWorkDays ( 2016,  1,  1,2016, 12, 31, &cnt ) == 1&& cnt == 254 );
  assert ( CountWorkDays ( 2015,  1,  1,2015, 12, 31, &cnt ) == 1&& cnt == 252 );
  assert ( CountWorkDays ( 2000,  1,  1,2016, 12, 31, &cnt ) == 1 && cnt == 4302 );
  assert ( CountWorkDays ( 2001,  2,  3, 2016,  7, 18, &cnt ) == 1&& cnt == 3911 );
  assert ( CountWorkDays ( 2014,  3, 27,2016, 11, 12, &cnt ) == 1&& cnt == 666 );
  assert ( CountWorkDays ( 2001,  1,  1,2000,  1,  1, &cnt ) == 0 );
  assert ( CountWorkDays ( 2001,  1,  1,2015,  2, 29, &cnt ) == 0 );

  //fit-wiki
  assert (   IsWorkDay ( 2008,   1,   2 ) );
  assert ( ! IsWorkDay ( 2018, 123, 124 ) );
  assert ( CountWorkDays ( 2000, -10, -10,    2200, 10,  1, &cnt ) == 0 );
  assert ( CountWorkDays ( 1999,  12,  31,    2000, 12, 31, &cnt ) == 0 );
  assert ( CountWorkDays ( 2001,   1,   1,    2000,  1,  1, &cnt ) == 0 );
  assert ( CountWorkDays ( 2001,   1,   1,    2015,  2, 29, &cnt ) == 0 );
  assert ( CountWorkDays ( 2000,  12,   2,    2000, 12,  3, &cnt ) == 1 && cnt ==          0 );
  assert ( CountWorkDays ( 2000,  12,   3,    2000, 12,  3, &cnt ) == 1 && cnt ==          0 );
  assert ( CountWorkDays ( 2000,  12,   1,    2000, 12,  3, &cnt ) == 1 && cnt ==          1 );
  assert ( CountWorkDays ( 2016,  11,   1,    2016, 11,  1, &cnt ) == 1 && cnt ==          1 );
  assert ( CountWorkDays ( 2000,  11,  30,    2000, 12,  3, &cnt ) == 1 && cnt ==          2 );
  assert ( CountWorkDays ( 2000,  11,  29,    2000, 12,  3, &cnt ) == 1 && cnt ==          3 );
  assert ( CountWorkDays ( 2000,  11,  28,    2000, 12,  3, &cnt ) == 1 && cnt ==          4 );
  assert ( CountWorkDays ( 2000,  11,  27,    2000, 12,  3, &cnt ) == 1 && cnt ==          5 );
  assert ( CountWorkDays ( 2004,  12,  26,    2004, 12, 31, &cnt ) == 1 && cnt ==          5 );
  assert ( CountWorkDays ( 2004,  12,  25,    2004, 12, 31, &cnt ) == 1 && cnt ==          5 );
  assert ( CountWorkDays ( 2016,  11,   1,    2016, 11, 17, &cnt ) == 1 && cnt ==         12 );
  assert ( CountWorkDays ( 2016,  11,   1,    2016, 11, 30, &cnt ) == 1 && cnt ==         21 );
  assert ( CountWorkDays ( 2008,   9,  30,    2008, 11, 11, &cnt ) == 1 && cnt ==         30 );
  assert ( CountWorkDays ( 2000,   5,   8,    2000, 12, 31, &cnt ) == 1 && cnt ==        163 );
  assert ( CountWorkDays ( 2015,   1,   1,    2015, 12, 31, &cnt ) == 1 && cnt ==        252 );
  assert ( CountWorkDays ( 2016,   1,   1,    2016, 12, 31, &cnt ) == 1 && cnt ==        254 );
  assert ( CountWorkDays ( 2014,   3,  27,    2016, 11, 12, &cnt ) == 1 && cnt ==        666 );
  assert ( CountWorkDays ( 2001,   2,   3,    2016,  7, 18, &cnt ) == 1 && cnt ==       3911 );
  assert ( CountWorkDays ( 2000,   1,   1,    2016, 12, 31, &cnt ) == 1 && cnt ==       4302 );
  assert ( CountWorkDays ( 2416,   4,   3,    2531,  1,  4, &cnt ) == 1 && cnt ==      29038 );
  //assert ( CountWorkDays ( 2076,   7,   4, 5094763,  6,  3, &cnt ) == 1 && cnt == 1288603482 );

  assert(CountWorkDays ( 2000, 5, 8, 2000, 12, 31, &cnt ) == 1 && cnt == 163);
  assert(CountWorkDays ( 2008, 9, 30, 2008, 11, 11, &cnt )==1&& cnt==30);
  assert(CountWorkDays ( 2416, 4, 3, 2531, 1, 4, &cnt )==1 && cnt==29038);
  assert (!IsWorkDay(2018, 123, 124));
  assert (CountWorkDays(2004, 12, 25, 2004,12,31,&cnt) == 1 && cnt == 5);
  assert (CountWorkDays(1999, 12, 31, 2000, 12, 31, &cnt) == 0 && cnt == 0);
  assert(CountWorkDays ( 2004, 12, 26, 2004, 12, 31, &cnt )==1 && cnt==5);
  assert(CountWorkDays ( 2004, 12, 25, 2004, 12, 31, &cnt )==1 && cnt==5);


    return 0;
}
#endif /* __PROGTEST__ */

